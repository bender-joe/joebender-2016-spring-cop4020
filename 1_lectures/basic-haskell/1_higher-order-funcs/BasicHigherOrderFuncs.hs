-- map, filter

map' :: (a -> b) -> [a] -> [b]
map' f xs = [ f x | x <- xs]

map'' :: (a -> b) -> [a] -> [b]
map'' f []     = []
map'' f (x:xs) = (f x):map'' f xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p xs = [x | x <- xs, p x]

filter'' :: (a -> Bool) -> [a] -> [a]
filter'' p []     = []
filter'' p (x:xs) =
  if p x then x:filter'' p xs else filter'' p xs

filter''' :: (a -> Bool) -> [a] -> [a]
filter''' p []     = []
filter''' p (x:xs)
  | p x       = x:filter''' p xs
  | otherwise = filter''' p xs

allPairs :: [a] -> [b] -> [(a,b)]
allPairs xs ys = [(x,y) | x <- xs, y <- ys]

-- [1,2,3] --> (1,2) (1,3) (2,3)

whatever :: (Ord a) => [a] -> [(a,a)]
whatever xs = [(x,y) | x <- xs, y <- xs, x < y]




                                   

