firstElement :: [a] -> a
firstElement [] = error "empty list"
firstElement (x:_) = x

secondElement (_:y1:_) = y1
-- secondElement (y:ys) = firstElement ys

function [(x:xs)] = x 
--function [x] = x 
